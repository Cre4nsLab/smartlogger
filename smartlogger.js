/**
 * @fileOverview A JavaScript logging wrapper around the console with logging level settings and remote logging to server
 * @author Ransford Okpoti <ranskills@yahoo.co.uk>
 * @docauthor Ransford Okpoti <ranskills@yahoo.co.uk>
 * @version 1.0.0
 */

/** @namespace */
var com = com || {};
/** @namespace */
com.cre4nslab = com.cre4nslab || {};

/** 
 * @singleton 
 * This is just 
 */
com.cre4nslab.SmartLogger = {


    /**
     * @class
     * @member com.cre4nslab.SmartLogger
     * 
     * @param {Integer} priority 
     * @param {String} name
     */
    LoggingLevel : function (priority, name) {
        /** 
         * The smaller the number, the higher the priority 
         * @property {Integer} priority
         */
        this.priority = priority;
        
        /** 
         * A unique name identifying this logging level 
         * @property {String} name
         */
        this.name = name;
        
        /** @return {String} */
        this.toString = function () { return this.name; };

        /** @return {Integer} */
        this.getPriority = function () { return this.priority; };
        
        /**
         * Determins if logs of this level should be logged
         * @return {Boolean} true if this level has a priority equal to or 
         *  greater than the currently set level, false otherwise
         */
        this.isLoggable = function () {
            var level = com.cre4nslab.SmartLogger.level;
            
            if (level === null) { return true; }
            return level.getPriority() >= this.priority; 
        };
        
        return this;
    },
    
    /** @private */
    level : null,
    
    /**
     * @member com.cre4nslab.SmartLogger
     * @see com.cre4nslab.SmartLogger.LoggingLevel
     * @param {LoggingLevel} level
     */
    setLevel: function (level) {
        if (!(level instanceof this.LoggingLevel)) { 
            throw new Error('level must be of type com.cre4nslab.SmartLogger.LoggingLevel but ' + typeof (level) + ' found.');
        }
        
        this.level = level;
    },
    
    /** 
     * Logs a DEBUG level message 
     * @member com.cre4nslab.SmartLogger
     */
    debug: function () {
		[].push.apply(arguments, [this.DEBUG]);
        this.logMessage.apply(this, arguments);
    },
    
    /** 
     * Logs a LOG level message 
     * @member com.cre4nslab.SmartLogger
     */
    log: function () {
		[].push.apply(arguments, [this.LOG]);
        this.logMessage.apply(this, arguments);
    },
    
    /** 
     * Logs an INFO level message 
     * @member com.cre4nslab.SmartLogger
     */
    info: function () {
		[].push.apply(arguments, [this.INFO]);
        this.logMessage.apply(this, arguments);
    },
    
    /** 
     * Logs a WARN(ing) level message 
     * @member com.cre4nslab.SmartLogger
     */
    warn: function () {
		[].push.apply(arguments, [this.WARN]);
        this.logMessage.apply(this, arguments);
    },
    
    /**
     * Logs an ERROR level message 
     * @member com.cre4nslab.SmartLogger
     */
    error: function () {
		[].push.apply(arguments, [this.ERROR]);
        this.logMessage.apply(this, arguments);
    },
    
    /**
     * Responsible for logging all messages
     * @member com.cre4nslab.SmartLogger
     * @private
     */
    logMessage: function () {
        var level = [].pop.apply(arguments),
            konsole = null;
        
        if (typeof console == 'undefined') return;
        if (!(level instanceof this.LoggingLevel && level.isLoggable())) {
            return;
        }
        
        switch (level.toString()) {
            case 'LOG':
                konsole = console.log;
                break;
                
            case 'DEBUG':
                konsole = console.debug;
                break;
                
            case 'INFO':
                konsole = console.info;
                break;
                
            case 'WARN':
                konsole = console.warn;
                break;
                
            case 'ERROR':
                konsole = console.error;
                break;
            default:
                throw new Error('Unknown logging level provided. Must be of type com.cre4nslab.SmartLogger.LoggingLevel');
        }
        
        if (konsole !== null) {
			if (typeof konsole.apply !== 'undefined'){
				konsole.apply(console, arguments);
			}
			else{
			    var stringArgs = [];
			    for(var i = 0; i < arguments.length; i++){
				    stringArgs[i] = "arguments[" + i + "]";
				}

			    eval("konsole(" + stringArgs.join(",") + ");");
			}
        }
        
    }
    
};

// Defining the logging level constants

/** 
 * @member com.cre4nslab.SmartLogger 
 * @property {com.cre4nslab.SmartLogger.LoggingLevel}
 */
com.cre4nslab.SmartLogger.ERROR = new com.cre4nslab.SmartLogger.LoggingLevel(1, 'ERROR');

/** 
 * @member com.cre4nslab.SmartLogger 
 * @property {com.cre4nslab.SmartLogger.LoggingLevel}
 */
com.cre4nslab.SmartLogger.WARN  = new com.cre4nslab.SmartLogger.LoggingLevel(2, 'WARN');

/** 
 * @member com.cre4nslab.SmartLogger 
 * @property {com.cre4nslab.SmartLogger.LoggingLevel}
 */
com.cre4nslab.SmartLogger.INFO  = new com.cre4nslab.SmartLogger.LoggingLevel(3, 'INFO');

/** 
 * @member com.cre4nslab.SmartLogger 
 * @property {com.cre4nslab.SmartLogger.LoggingLevel}
 */
com.cre4nslab.SmartLogger.DEBUG = new com.cre4nslab.SmartLogger.LoggingLevel(4, 'DEBUG');

/** 
 * @member com.cre4nslab.SmartLogger 
 * @property {com.cre4nslab.SmartLogger.LoggingLevel}
 */
com.cre4nslab.SmartLogger.LOG   = new com.cre4nslab.SmartLogger.LoggingLevel(5, 'LOG');
