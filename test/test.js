
module("com.cre4nslab.SmartLogger",{
    setup: function(){
        ok(typeof com.cre4nslab.SmartLogger !== 'undefined', 'com.cre4nslab.SmartLogger is defined');
        this.logger = com.cre4nslab.SmartLogger;
    }
})

test("level defaults to null", function() {
    equal(this.logger.level, null, 'level defaults to null');
//    ok(true)
});

test("Checking wrong parameter type of setLevel()", function() {
    raises(function(){ this.logger.setLevel(true) },'Booleans not accepted');
    raises(function(){ this.logger.setLevel(1) },'Integers not accepted');
    raises(function(){ this.logger.setLevel(101.8) },'Doubles not accepted');
    raises(function(){ this.logger.setLevel(null) },'Nulls not accepted');
    raises(function(){ this.logger.setLevel('') },'Strings not accepted');
    raises(function(){ this.logger.setLevel({}) },'Unspecified objects not accepted');
    raises(function(){ this.logger.setLevel() },'The level to set is expected');
});


test("Checking correct parameter type of setLevel()", function() {
    equal(typeof this.logger.setLevel(new com.cre4nslab.SmartLogger.LoggingLevel(1, 'PARAMETER CHECK LEVEL')) , 'undefined', 'Only com.cre4nslab.SmartLogger.LoggingLevel instances are accepted');
});

test("Test instances of all logging levels are of type com.cre4nslab.SmartLogger.LoggingLevel", function() {
    ok(this.logger.ERROR instanceof com.cre4nslab.SmartLogger.LoggingLevel,  'ERROR is of type com.cre4nslab.SmartLogger.LoggingLevel');
    ok(this.logger.WARN  instanceof com.cre4nslab.SmartLogger.LoggingLevel,  'WARN is of type com.cre4nslab.SmartLogger.LoggingLevel');
    ok(this.logger.INFO  instanceof com.cre4nslab.SmartLogger.LoggingLevel,  'INFO is of type com.cre4nslab.SmartLogger.LoggingLevel');
    ok(this.logger.DEBUG instanceof com.cre4nslab.SmartLogger.LoggingLevel,  'DEBUG is of type com.cre4nslab.SmartLogger.LoggingLevel');
    ok(this.logger.LOG   instanceof com.cre4nslab.SmartLogger.LoggingLevel,  'LOG is of type com.cre4nslab.SmartLogger.LoggingLevel');
});



//test("Testing the class", function() {
//    ok(this.logger.LOG   instanceof logger.LoggingLevel,  'LOG is of type com.cre4nslab.SmartLogger.LoggingLevel');
//});

module("Test com.cre4nslab.SmartLogger.LoggingLevel class", {
    params: {
        LEVEL_NO: 1,
        LEVEL_NAME: 'TEST_LEVEL'
    },
    
    setup: function() {
        this.loggingLevel = new com.cre4nslab.SmartLogger.LoggingLevel(this.params.LEVEL_NO, this.params.LEVEL_NAME);
        ok(this.loggingLevel !== null, 'LoggingLevel instance created successfully');
    }
});

test("Instance methods", function() {
    expect(3);
    
    equal(this.loggingLevel.getPriority(), this.params.LEVEL_NO, 'getPriority() passed');
    equal(this.loggingLevel.toString(), this.params.LEVEL_NAME, 'toString() passed');
});
