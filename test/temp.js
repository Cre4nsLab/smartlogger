/**
* Adds numbers
* @returns {Number}
*/
function sum(){
	var total = 0;
	
	for(var i=0, j=arguments.length; i<j; i++){
		total += arguments[i];		
	}
	
	return total;
}

test("testSumWithValidArguments", function() {
    equal(sum(2,2), 4, '2+2 = 4');
    equal(sum(2,1, 5, 6), 4, '2 + 1 + 5 + 6 = 14');
});


test("testSumWithNoArguments", function() {
    raises(function(){ sum(); }, 'Exception thrown');
});

test("testSumWithWrongDataToResultInException", function() {
    raises(sum(2,{3}),'Object rejected');
});


model = model || {}
exception = exception || {}
// Example 2

/**
 * Creates a new Company instance
 * @class
 *
 * @param {Object} param
 * @param {String} param.name The name of the company
 * @param {Integer} param.year The year the company was founded
 */
model.Company = function(param){
	param = param || {}
	
	this.name = param.name || '';
	this.yearFounded = param.year || 0;

	this.departments = [];
	
	/**
	 *	@param {Department} 
	 *	@throws {DepartmentExistException} Department already exist
	 */
	addDepartment = function(department){
		Array.push(this.departments, department)
	}	
	
	return this;
}

/**
 * Creates a new Department
 * @class
 * 
 * @param {String} name
 * @param {Integer} [name=AUTO_GENERATED]
 */
model.Department = function(name,id){
	this.id = id ; // put autogeneration here
	this.name = name;
	this.company = null;
	
	return this;
}

model.Employee = function(data){
	data = data || {}
	
	this.firstName = data.firstName || '';
	this.lastName = data.lastName || '';
	this.birthYear = param.birthYear || 0;
	
	this.getAge = function(){ return 0;}
	
	this.getName = function(){
		return this.toString()
	}

	this.toString = function(){
		return this.firstName + ' ' + this.lastName;
	}
	
}

exception.DepartmentExistException = function(value){
	this.value = value;
	this.name = 'exception.DepartmentExistException';
	this.message = ' department already exist';
	
	this.toString = function(){
		return this.value + this.message;
	}
}

// Tests
1. No of employees due for retirement
2. Adding duplicate department should fail
3. Check number of departments for added to a company